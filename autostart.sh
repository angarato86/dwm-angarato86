#!/bin/bash

function run {
 if ! pgrep $1 ;
  then
    $@&
  fi
}

#run xrandr --output DP-1 --primary --mode 2560x1440 --rate 144 --pos 0x0 --rotate normal --output DP-2 --off --output DP-3 --mode 2560x1440 --rate 144 --pos 2560x0 --rotate normal --output HDMI-1 --off

run dwmblocks &
run "nm-applet"
run "pamac-tray"
run "xfce4-power-manager"
run "/usr/lib/xfce4/notifyd/xfce4-notifyd"
run "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
picom -b  --config ~/.config/picom.conf &
run "numlockx on"
run "volumeicon"
sxhkd -c ~/.config/sxhkd/sxhkdrc &
run "nitrogen --restore"
run "nextcloud"
run "protonmail-bridge"
